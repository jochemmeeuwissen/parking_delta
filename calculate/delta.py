import googlemaps
import pytz
from datetime import datetime,timedelta

def loc_to_utc(datetime,tz_str,dst=None):
    """
    input: datetime object, corresponding timezone string, and whether the time is in daylight saving time
    output: same time, but in utc as datetime object
    """
    local_tz=pytz.timezone(tz_str)
    utc_dt=local_tz.localize(datetime, is_dst=dst).astimezone(pytz.utc)
    return utc_dt




def calc_delta(client,dep_time,arr_time,destination,request,exclude=[],timezone='Europe/Amsterdam'):
    """
    Input:  client - google maps client
            dep_time - list of timedeltas where the first of the list is the default time for departure time
            arr_time - list of timedeltas where the first of the list is the default time for arrival time
            destination - destination string formatted as 'streetname housenumber, city'
            exclude - list of public transport services to exclude
            request - dictionary containing request parameters
            
    Output: response - dictionary with travel times, delta and comment.
    """
    
    if request['dst']=='Yes':
        dst=True
    elif request['dst']=='No':
        dst=None
    
    # Process request data into home address and date 
    home = request['street']+' '+request['housenumber']+', '+request['city']
    day=datetime.strptime(request['datum'], '%Y-%m-%d')
    
    # Merge the dep_time and arr_time timedeltas with the day into datetimes
    dep_time=[loc_to_utc((day+dt),timezone,dst=dst) for dt in dep_time]
    arr_time=[loc_to_utc((day+dt),timezone,dst=dst) for dt in arr_time]
    
    #initialize response
    response={}
    
    # Car to destination - use distance_matrix
    car_to=0
    try:
        car_to = client.distance_matrix(origins=home,
                                       destinations=destination, 
                                       mode="driving", 
                                       arrival_time=arr_time[0],
                                       language="NL"
                                       )['rows'][0]['elements'][0]['duration']['value']/60
        
    except Exception as e:
        print(e)
        pass 
         
    # Car back home - use distance_matrix
    car_back=0
    try:
        car_back = client.distance_matrix(origins=destination,
                                       destinations=home, 
                                       mode="driving", 
                                       departure_time=dep_time[0],
                                       language="NL"
                                       )['rows'][0]['elements'][0]['duration']['value']/60
        
    except Exception as e:
        print(e)
        pass
    
    # Public transport to destination - use directions API such that exceptions for PT services can be made
    PT_to=[]
    for dt in arr_time:
        try:            
            directions = client.directions(origin=home,
                                     destination=destination,
                                     mode="transit",
                                     arrival_time=dt,
                                     language="NL") 
            if any(PTs in str(directions) for PTs in exclude):
                PT_to=PT_to
            else:
                PT_to.append(directions[0]['legs'][0]['duration']['value']/60)
        except Exception as e:
            
            pass
    
    if len(PT_to)>0:
        PT_to=min(PT_to)
    else:
        PT_to=0
        
    # Public transport back home - use directions API such that exceptions for PT services can be made
    PT_back=[]
    for dt in dep_time:
        try:            
            directions = client.directions(origin=destination,
                                     destination=home,
                                     mode="transit",
                                     departure_time=dt,
                                     language="NL") 
            if any(PTs in str(directions) for PTs in exclude):
                PT_back=PT_back
            else:
                PT_back.append(directions[0]['legs'][0]['duration']['value']/60)
        except Exception as e:
            
            pass
    
    if len(PT_back)>0:
        PT_back=min(PT_back)
    else:
        PT_back=0
    
    
    if min([car_to,car_back])>0:
        car_time=(car_to+car_back)/2
    else:
        car_time=max([car_to,car_back])
    

    
    if min([PT_to,PT_back])>0:
        PT_time=(PT_to+PT_back)/2
    else:
        PT_time=max([PT_to,PT_back])
        
    if (car_time==0)&(PT_time==0):
        response['message']='No delta was calculated: missing car time and public transport time'
    elif car_time==0:
        response['message']='No delta was calculated: missing car time'
    elif PT_time==0:
        response['message']='No delta was calculated: missing public transport time'
    else:
        response['message']='Ok'
    
    response['car_time']=car_time
    response['public_transport_time']=PT_time
    response['delta']=PT_time-car_time
      
    
    return response