import os
from dotenv import load_dotenv
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv('.env')

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'from-dorp-to-stad'
    USER=os.environ.get('DELTA_USER')
    PW=os.environ.get('DELTA_PASSWORD')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False