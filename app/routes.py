from flask import render_template, flash, redirect, url_for, request
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.urls import url_parse
from app import app, db
from app.forms import LoginForm, DeltaCalculatorForm
from app.models import User
import googlemaps
import os
from datetime import datetime,timedelta
from calculate.delta import calc_delta

gmaps = googlemaps.Client(key=os.environ['GMAPS_KEY'])

# departure times from work to home --local timezone ,to be converted to utc
dep_time = [timedelta(hours=17,minutes=0)
            ,timedelta(hours=17,minutes=10)
            ,timedelta(hours=17,minutes=20)
            ,timedelta(hours=17,minutes=30)]

# arrival times at work from home --local timezone ,to be converted to utc
arr_time = [timedelta(hours=8,minutes=30)
            ,timedelta(hours=8,minutes=40)
            ,timedelta(hours=8,minutes=50)
            ,timedelta(hours=9,minutes=0)]

# work location
office = "Thomas R. Malthusstraat 1, Amsterdam"

exclude = ['Thalys','Flix']





@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    form = DeltaCalculatorForm()
    if form.validate_on_submit():
        return redirect(url_for('delta'), code=307)
    return render_template('index.html', title='Home', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/delta',methods = ['POST', 'GET'])
@login_required
def delta():
    if request.method == 'POST':
        result = request.form        
        delta=calc_delta(gmaps,dep_time,arr_time,office,result,exclude)
        return render_template('delta.html',delta = delta)