from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')
    
class DeltaCalculatorForm(FlaskForm):
    street = StringField('Street', validators=[DataRequired()])
    housenumber = IntegerField('Housenumber',validators=[DataRequired()])
    housenumber_addition=StringField('Housenumber addition')
    city = StringField('City', validators=[DataRequired()])
    datum = DateField('Date', format='%Y-%m-%d', validators=[DataRequired()])
    dst = SelectField('Daylight saving time', choices = [('Yes', 'Yes'), ('No', 'No')])
    submit = SubmitField('Calculate Delta')